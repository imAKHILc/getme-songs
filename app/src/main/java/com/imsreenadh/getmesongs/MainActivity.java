package com.imsreenadh.getmesongs;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.view.inputmethod.InputMethodManager;

import java.io.IOException;
import java.lang.String;
//JSoup for web scrapping


public class MainActivity extends AppCompatActivity{


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button search_song_button = (Button)findViewById(R.id.search_song_button);
        search_song_button.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View v) {
                        hideKeyboard();
                        try {
                            searchSong();
                        } catch (IOException e) {
                            System.out.println("searchSong() exception thrown: "+e);
                        }
                    }
                }
        );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();

        //Handling menu options
        switch (item.getItemId()) {
            case R.id.action_settings:
                showSettings();
                return true;
            case R.id.action_about_gms:
                showAbout();
                return true;
            case R.id.action_refine_search:
                showRefine();
                //setContentView(R.layout.magic_search);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showRefine() {
        toaster("Refine");
    }

    private void showAbout() {
        toaster("About");
    }

    private void showSettings() {
        toaster("Settings");
    }

    public void toaster(String s){
        Context context = getApplicationContext();
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, s, duration);
        toast.show();
    }
    private void hideKeyboard(){
        InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);

        inputManager.hideSoftInputFromWindow(
                (null == getCurrentFocus()) ? null : getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
    }
    public boolean hasConnection() {
        ConnectivityManager cm = (ConnectivityManager)getSystemService(
                Context.CONNECTIVITY_SERVICE);

        NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()) {
            return true;
        }

        NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()) {
            return true;
        }

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null && activeNetwork.isConnected()) {
            return true;
        }

        return false;
    }
    public void searchSong() throws IOException{
        if(hasConnection()){
            //perform search
            EditText search_song_input = (EditText)findViewById(R.id.search_song_input);
            String search_input = "No input";//initialised and for debugging, checked
            /*
            Validate null entry by the user and get input otherwise
            trim() trims of whitespaces and similar character so that entry count remains 0
             */
            if( search_song_input.getText().toString().trim().length() == 0 ) {
                Context context = getApplicationContext();
                int duration = Toast.LENGTH_SHORT;
                Toast toast = Toast.makeText(context, "Please enter a Song name!", duration);
                toast.show();
                // System.out.println("inside if, null entry check"); --> debug purpose,checked
            }
            else{
                new MagicSearch().new GetResultBg().execute(search_song_input.getText().toString());
            }
            // System.out.println("After validation: "+ search_input); --> debug purpose,checked
        }
        else{
            //alert the user using toast
            Context context = getApplicationContext();
            CharSequence text = "Sorry, you have no Internet access!!";
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        }
    }
}
