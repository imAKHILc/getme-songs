-keep class null {
    <init>();
    void attachBaseContext(android.content.Context);
}
-keep class com.imsreenadh.getmesongs.MainActivity { <init>(); }
-keep class com.imsreenadh.getmesongs.GetmesongsSplash { <init>(); }
-keep class com.imsreenadh.getmesongs.MagicSearch { <init>(); }
-keep public class * extends android.app.backup.BackupAgent {
    <init>();
}
-keep public class * extends java.lang.annotation.Annotation {
    *;
}
